<?php

public function post_confirm() {
  $id = Input::get('service_id');
  $driverId = Input::get('driver_id');
  $service = Service::find($id);

  if (is_null($service)) {
    return Response::json(array('error' => '3'));
  }
  if ($service->status_id == '6') {
    return Response::json(array('error' => '2'));
  }
  if (!is_null($service->driver_id) && !$service->status_id == '1') {
    return Response::json(array('error' => '1'));
  }

  $driverTmp = Driver::update($driverId, array('available' => '0'));

  $service = Service::update($id, array(
    'driver_id' => $driverId,
    'status_id' => '2',
    'car_id' => $driverTmp->car_id
  ));

  // Notify user
  if (!empty($service->user->uuid)) {
    push_notification($service->user->type, 'Tu serivicio ha sido confirmado!',
      $service->user->uuid, $id);
  }

  return Response::json(array('error' => '0'));
}

/**
 * Sends push notify
 *
 * @param  [string]  $type     [description]
 * @param  [string]  $message  [description]
 * @param  [string]  uuid      [description]
 * @param  [integer] serviceId [description]
 */
public function push_notification($type, $message, uuid, serviceId) {
  $push = Push::make();
  if ($type == '') { // iPhone
    $push->ios($uuid, $message, 1, 'honk.wav', 'Open', array('serviceId' => $serviceId));
  } else {
    $push->android2($uuid, $message, 1, 'default', 'Open', array('serviceId' => $serviceId));
  }
}
