Grability
===================

Project structure

web/src

 - Controllers/GraphController.php
	This class contains the entire business rule responsible for responding to calls from the client.
 - Models/graph/GraphModel.php
	This class is responsible for manipulating the requested data to the Graph entity database.
 - Models/graph/GraphQuery.php
	Class that contains all the query string of Graph entity.
 - Models/graph/GraphEntity.php
   Representational class of the Graph entity.
 - Messages/GraphMessage.php
   Class that contains all Graph response messages to the client.

web/libs/database
	In the files contained within this folder is the encapsulation of all the methods and classes necessary to make queries to a relational database in this case postgressql

web/libs
	In general you will find all the necessary and useful files for the proper functioning of the framework
