<?php

$version = "/v1";

$app->post($version.'/graph', 'Grability\Controllers\GraphController:calculate');
$app->get($version.'/graph', 'Grability\Controllers\GraphController:getGraphs');
