<?php

return [
  'settings' => [
    'displayErrorDetails' => false,
    'logger' => [
      'name' => 'slim-app',
      'path' => __DIR__ . '/../logs/app.log',
    ]
  ],
];
