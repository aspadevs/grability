<?php

/**
 * ORM class in charge of the entire transactional part based on data.
 * Pattern used factory.
 *
 * @class GrabilityDataBase
 * @uses PDO
 * @author sebastian.delaroche
 * @since 18/11/2017
 */

namespace Grability\Libs\DataBase;

use \PDO as PDO;
use \PDOException as PDOException;
use Grability\Libs\DataBase\GrabilityDataBaseException as GrabilityDataBaseException;


class GrabilityDataBase
{
	/**
	 * @var string $database
	 * @access private
	 */
 	private $dataBase;

 	/**
	 * @var string $host
	 * @access private
	 */
 	private $host;

 	/**
	 * @var string $user
	 * @access private
	 */
 	private $user;

 	/**
	 * @var string $password
	 * @access private
	 */
 	private $password;

 	/**
	 * @var string $adapter
	 * @access private
	 */
 	private $adapter;

 	/**
	 * @var GrabilityDataBase $instancia
	 * @access private
	 */
 	private static $instancia;

 	/**
	 * @var PDO $conexion
	 * @access private
	 */
 	private $conexion;

 	/**
	 * @var integer $transLevel
	 * @access private
	 */
  private $transLevel = 0;


	private function __construct($oAthConnection = false) {
    $config = file_get_contents(dirname(__DIR__) . "/database/settings.database.json");
    $data = json_decode($config, true);

  	$this->database = $data['database'];
  	$this->host = $data['host'];
  	$this->user = $data['user'];
  	$this->password = $data['password'];
  	$this->adapter = $data['adapter'];
	}

  /**
   * Singleton DataBase of Resource
   *
   * @access public
   * @return GrabilityDataBase PDO
   * @author sebastian.delaroche
   */
	public static function getInstance() {
    if(!self::$instancia instanceof self) {
      self::$instancia = new self;
    }
    return self::$instancia;
	}

  /**
   * Make the connection to the database.
   *
   * @access public
   * @return GrabilityDataBase PDO
   * @author sebastian.delaroche
   */
	public function conexion() {
		if($this->conexion) return $this;
		try {
			$this->conexion = new PDO(
        $this->adapter . ':dbname=' . $this->database . ';host=' . $this->host,
        $this->user,
        $this->password
      );
			$this->conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return $this;
		} catch (PDOException $e) {
  		echo 'Fail connection: ' . $e->getMessage();
  		exit();
		}
	}

  /**
   * Validate the database engine.
   *
   * @access public
   * @return boolean
   * @author sebastian.delaroche
   */
	private function nestable() {
    return in_array($this->conexion->getAttribute(PDO::ATTR_DRIVER_NAME), array("pgsql", "mysql"));
  }

  /**
   * Execute query and return an array of object.
   *
   * @access public
   * @param string $sql
   * @param array $params
   * @return array PDO
   * @author sebastian.delaroche
   */
	public function fetchAll($sql, $params = array()) {
    try {
      $query = $this->conexion->prepare($sql);
      $query->execute($params);
      return $query->fetchAll(PDO::FETCH_CLASS);
    } catch (PDOException $e) {
      throw new GrabilityDataBaseException($e);
    }
  }

  /**
   * Execute query and return an object.
   *
   * @access public
   * @param string $sql
   * @param array $params
   * @return array PDO
   * @author sebastian.delaroche
   */
	public function fetchRow($sql, $params = array()) {
    try {
      $query = $this->conexion->prepare($sql);
      $query->execute($params);
      return $query->fetchObject();
    } catch (PDOException $e) {
      throw new GrabilityDataBaseException($e);
    }
	}

  /**
   * Execute query and return a single value.
   *
   * @access public
   * @param string $sql
   * @param array $params
   * @return array PDO
   * @author sebastian.delaroche
   */
	public function fetchOne($sql, $params = array()) {
    try {
      $query = $this->conexion->prepare($sql);
      $query->execute($params);
      return $query->fetchColumn();
    } catch (PDOException $e) {
    throw new GrabilityDataBaseException($e);
    }
	}

  /**
   * Insert an element in the database and return its id.
   *
   * @access public
   * @param string $sql
   * @param array $params
   * @return array PDO
   * @author sebastian.delaroche
   */
	public function insert($sql, $params = array()) {
    try {
      $query = $this->conexion->prepare($sql);
      $query->execute($params);
      return $this->conexion->lastInsertId();
    } catch (PDOException $e) {
      echo $e->getMessage();
      throw new GrabilityDataBaseException($e);
    }
	}

  /**
   * Execute a query.
   *
   * @access public
   * @param string $sql
   * @param array $params
   * @return array PDO
   * @author sebastian.delaroche
   */
	public function query($sql, $params = array()) {
    try {
      $query = $this->conexion->prepare($sql);
      $query->execute($params);
      return $query->rowCount();
    } catch (PDOException $e) {
      throw new GrabilityDataBaseException($e);
    }
	}

  /**
   * Initialize a transaction.
   *
   * @access public
   * @return void
   * @author sebastian.delaroche
   */
	public function beginTransaction() {
    if(!$this->nestable() || $this->transLevel == 0) {
      $this->conexion->beginTransaction();
    } else {
      $this->conexion->exec("SAVEPOINT LEVEL{$this->transLevel}");
    }
    $this->transLevel++;
	}

  /**
   * Save the changes in database.
   *
   * @access public
   * @return void
   * @author sebastian.delaroche
   */
	public function commit() {
    $this->transLevel--;
    if(!$this->nestable() || $this->transLevel == 0) {
      $this->conexion->commit();
    } else {
      $this->conexion->exec("RELEASE SAVEPOINT LEVEL{$this->transLevel}");
    }
	}

  /**
   * Reverts the changes in database.
   *
   * @access public
   * @return void
   * @author sebastian.delaroche
   */
	public function rollback() {
    $this->transLevel--;
    if($this->transLevel < 0) {
      return;
    }
    if(!$this->nestable() || $this->transLevel == 0) {
      $this->conexion->rollback();
    } else {
      $this->conexion->exec("ROLLBACK TO SAVEPOINT LEVEL{$this->transLevel}");
    }
	}
  
}

?>
