<?php

/**
 * Wrapper for PDOException
 *
 * @author sebastian.delaroche
 * @since 18/11/2017
 */

namespace Grability\Libs\DataBase;


class GrabilityDataBaseException extends \PDOException
{

	const ERROR_FOREIGN_KEY = 23000;

	/**
	 * @constructor
	 * @param PDOException $e
	 */
  public function __construct(\PDOException $e) {
	$this->code = $e->getCode();
    $this->message = $this->messageByTypeError($e->getCode());
  }

 /**
	* Set the appropriate message according to the database error.
	*
	* @access private
	* @param Integer $code
	* @return String $message
	*/
  private function messageByTypeError($code) {

  	$message = "";

  	switch ($code) {
  		case self::ERROR_FOREIGN_KEY:
  			$message = "Error de foraneas (mejorar mensaje).";
  			break;

  		default:
  			$message = "Error interno en base de datos por favor comunicarse con el area de desarrollo y dar soluciòn a este error y mejorar la experiencia de usuario.";
  			break;
  	}

  	return $message;
  }

}


?>
