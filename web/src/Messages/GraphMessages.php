<?php

/**
 * Has all reference message to graph
 *
 * @class GraphMessages
 * @author sebastian.delaroche
 * @since 18/11/2017
 */

namespace Grability\Messages;


class GraphMessages
{
	public static $VALIDATE_EMPTY_FILED = "The '%s' field can not be empty.";

	public static $CALCULATE_SUCCESSFUL = "The time has been calculated satisfactorily.";

	public static $GET_ALL_GRAPHS = "Has been listed successful all graphs.";

}
