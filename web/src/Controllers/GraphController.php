<?php

/**
 * @class GraphController
 * @author sebastian.delaroche
 * @since 18/11/2017
 */

namespace Grability\Controllers;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

use \Fisharebest\Algorithm\Dijkstra as Dijkstra;

use \Grability\Libs\DataBase\GrabilityDataBase;
use \Grability\Messages\GraphMessages;
use \Grability\Models\Model\GraphModel;


class GraphController
{

	/**
	 * Property that the container will have with all injections of dependances.
	 *
	 * @var Container $ci
	 * @access private
	 */
	private $ci;

	/**
	 * Property that will contain the object of the GraphModel model.
	 *
	 * @var GraphModel $oGraph
	 * @access private
	 */
	private $oGraph;

	/**
	 * @author sebastian.delaroche
	 * @param Container $ci
	 */
	public function __construct($ci) {
		$this->ci = $ci;
		$this->oGraph = new GraphModel(GrabilityDataBase::getInstance());
	}

	/**
	 * Method responsible for calculating the time it takes
	 * to get a cat from a point X to a point Y in the shortest
	 * possible time
	 *
	 * @access public
	 * @param  Request $request
	 * @param  Response $response
	 * @return Response json
	 * @author sebastian.delaroche
	 */
	public function calculate(Request $request, Response $response) {
		$oResponse = new \stdClass();
		$paramsBody = $request->getParsedBody();

		try {
			$this->validate($paramsBody);

			$graph = $paramsBody['graph'];
			$initialVertix = $paramsBody['initial_vertix'];
			$finalVertix = $paramsBody['final_vertix'];

			$algorithm = new Dijkstra($graph);
			$minimumTime = $this->getMinimumTimeTravel(
				$algorithm->shortestPaths($initialVertix, $finalVertix),
				$graph
			);

			$this->oGraph->setJsonGraph(json_encode($graph, JSON_UNESCAPED_UNICODE));
			$this->oGraph->setInitialVertix($initialVertix);
			$this->oGraph->setFinalVertix($finalVertix);
			$this->oGraph->setTimeMinimum($minimumTime);

			$this->oGraph->insert();

			$oResponse->data = $minimumTime;
			$oResponse->message = GraphMessages::$CALCULATE_SUCCESSFUL;
			$oResponse->code = 200;

		} catch (\Exception $e) {

			$oResponse->data = null;
			$oResponse->message = $e->getMessage();
			$oResponse->code = 500;

		} finally {

			return $response->withJson($oResponse, $oResponse->code);

		}
	}

	/**
	 * Gets all calculates registered
	 *
	 * @access public
	 * @param  Request $request
	 * @param  Response $response
	 * @return Response json
	 * @author sebastian.delaroche
	 */
	public function getGraphs(Request $request, Response $response) {
		$oResponse = new \stdClass();

		try {

			$oResponse->data = $this->oGraph->getAll();
			$oResponse->message = GraphMessages::$GET_ALL_GRAPHS;
			$oResponse->code = 200;

		} catch (\Exception $e) {

			$oResponse->data = null;
			$oResponse->message = $e->getMessage();
			$oResponse->code = 500;

		} finally {

			return $response->withJson($oResponse, $oResponse->code);

		}
	}

	/**
	 * Validate all fields needly
	 *
	 * @access private
	 * @param  Array $args
	 * @author sebastian.delaroche
	 */
	private function validate(array $args) {
		if(is_null($args['graph']) || empty($args['graph'])) {
			throw new \Exception(sprintf(GraphMessages::$VALIDATE_EMPTY_FILED, 'graph'));
		}
		if(is_null($args['initial_vertix']) || empty($args['initial_vertix'])) {
			throw new \Exception(sprintf(GraphMessages::$VALIDATE_EMPTY_FILED, 'initial_vertix'));
		}
		if(is_null($args['final_vertix']) || empty($args['final_vertix'])) {
			throw new \Exception(sprintf(GraphMessages::$VALIDATE_EMPTY_FILED, 'final_vertix'));
		}
	}


	/**
	 * Calculates the minimum time in travel all vertex
	 *
	 * @access private
	 * @param  Array $pointsTraveled
	 * @param  Array $graph
	 * @return Interger
	 * @author sebastian.delaroche
	 */
	private function getMinimumTimeTravel(array $pointsTraveled, array $graph) {
		$minimunTime = 0;
		for ($i = 0; $i < count($pointsTraveled); $i++) {
			$tempTime = 0;
			for ($j = 0; $j < count($pointsTraveled[$i]); $j++) {
				$currentVertex = $pointsTraveled[$i][$j];
				$selectedVertex = $pointsTraveled[$i][$j + 1];
				$time = $graph[$currentVertex][$selectedVertex];
				$tempTime += !is_null($time) ? $time : 0;
			}
			if ($minimunTime === 0 || $tempTime < $minimunTime) {
				$minimunTime = $tempTime;
			}
		}

		return $minimunTime;
	}

}

?>
