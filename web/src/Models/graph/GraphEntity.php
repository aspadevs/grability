<?php

/**
 * Entity associated to the GraphModel
 *
 * @class GrahpEntity
 * @author sebastian.delaroche
 * @since 18/11/2017
 */

namespace Grability\Models\Entity;


class GraphEntity
{

	// Attributes -----------------------------/

	/**
	 * @var integer $id
	 * @access protected
	 */
	protected $id;

	/**
	 * @var string $json_graph
	 * @access protected
	 */
	protected $json_graph;

	/**
	 * @var string $initial_vertix
	 * @access protected
	 */
	protected $initial_vertix;

	/**
	 * @var string $final_vertix
	 * @access protected
	 */
	protected $final_vertix;

	/**
	 * @var string $time_minimum
	 * @access protected
	 */
	protected $time_minimum;

	/**
	 * @var GrabilityDataBase $database
	 * @access protected
	 */
	protected $database;


	// Properties -----------------------------/

	/**
   * @access public
   * @param  integer $id
   * @return void
   * @author sebastian.delaroche
   */
	public function setId($id) { $this->id = $id; }

	/**
   * @access public
   * @return integer $id
   * @author sebastian.delaroche
   */
	public function getId() { return $this->$id; }

	/**
   * @access public
   * @param  string $json_graph
   * @return void
   * @author sebastian.delaroche
   */
	public function setJsonGraph($json_graph) { $this->json_graph = $json_graph; }

	/**
   * @access public
   * @return string $json_graph
   * @author sebastian.delaroche
   */
	public function getJsonGraph() { return $this->json_graph; }

	/**
   * @access public
   * @param  string $initial_vertix
   * @return void
   * @author sebastian.delaroche
   */
	public function setInitialVertix($initial_vertix) { $this->initial_vertix = $initial_vertix; }

	/**
   * @access public
   * @return string $initial_vertix
   * @author sebastian.delaroche
   */
	public function getInitialVertix() { return $this->initial_vertix; }

	/**
   * @access public
   * @param  string $final_vertix
   * @return void
   * @author sebastian.delaroche
   */
	public function setFinalVertix($final_vertix) { $this->final_vertix = $final_vertix; }

	/**
   * @access public
   * @return string $final_vertix
   * @author sebastian.delaroche
   */
	public function getFinalVertix() { return $this->final_vertix; }

	/**
   * @access public
   * @param  string $time_minimum
   * @return void
   * @author sebastian.delaroche
   */
	public function setTimeMinimum($time_minimum) { $this->time_minimum = $time_minimum; }

	/**
   *
   * @access public
   * @return string $time_minimum
   * @author sebastian.delaroche
   */
	public function getTimeMinimum() { return $this->time_minimum; }

}

?>
