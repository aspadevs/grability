<?php

/**
 * Has all reference query strings to graph
 *
 * @class GraphQuery
 * @author sebastian.delaroche
 * @since 18/11/2017
 */

namespace Grability\Models\Query;

class GraphQuery
{

	// INSERT -----------------------------------------------------------------------------------------

	public static $INSERT_GRAPH = "INSERT INTO graph (json_graph, initial_vertix, final_vertix, time_minimum) VALUES (?, ?, ?, ?);";

	// SELECT -----------------------------------------------------------------------------------------

	public static $GET_ALL = "SELECT id, json_graph, initial_vertix, final_vertix, time_minimum FROM graph;";

}

?>
