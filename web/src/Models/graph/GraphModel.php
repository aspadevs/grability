<?php

/**
 * Represetation of the graph entity in database
 *
 * @class GraphModel
 * @author sebastian.delaroche
 * @since 18/11/2017
 */

namespace Grability\Models\Model;

use Grability\Libs\DataBase\GrabilityDataBase;
use Grability\Libs\DataBase\GrabilityDataBaseException;
use Grability\Messages\GraphMessages;

use Grability\Models\Entity\GraphEntity;
use Grability\Models\Query\GraphQuery;


class GraphModel extends GraphEntity
{

	/**
	 * @author sebastian.delaroche
	 * @param DataBase $conexion
	 */
	public function __construct(GrabilityDataBase $DB) {
		$this->database = $DB;
	}

	/**
	 * Registers all information about a graph
	 *
	 * @author sebastian.delaroche
	 * @since 18/11/2017
	 * @return void
	 */
	public function insert() {
		try {
			$oConexion = $this->database->conexion();
			$oConexion->beginTransaction();

			$this->id = $oConexion->insert(
				GraphQuery::$INSERT_GRAPH,
				array($this->json_graph, $this->initial_vertix, $this->final_vertix, $this->time_minimum)
			);

			$oConexion->commit();

		} catch (GrabilityDataBaseException $e) {

			$oConexion->rollback();
			throw new \Exception($e->getMessage());

		} catch (\Exception $e) {

			$oConexion->rollback();
			throw new \Exception($e->getMessage());
		}
	}

	/**
	 * Gets a list of all graphs register in database
	 *
	 * @author sebastian.delaroche
	 * @since 11/09/2016
	 * @return void
	 */
	public function getAll() {
		try {
			$oConexion = $this->database->conexion();
			return $oConexion->fetchAll(GraphQuery::$GET_ALL);
		} catch (GrabilityDataBaseException $e) {
			throw new \Exception($e->getMessage());
		}
	}

}

?>
