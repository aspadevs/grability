Grability
===================

This project is deployed in heroku, the two available services are

POST https://grability.herokuapp.com/v1/graph
The data it receives is for example:

    {
	    "graph": {
		    "A": {
			    "B": 10,
			    "C": 10
		    },
		    "B": {
			    "D": 10
		    },
		    "C": {
			    "D": 10
		    }
	    },
	    "initial_vertix": "A",
	    "final_vertix": "B"
    }

GET https://grability.herokuapp.com/v1/graph
This resource returns all the calculations recorded by each graph